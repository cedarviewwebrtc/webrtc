require('dotenv').config()
const express = require("express");
const app = express();
const {port} = require("./config");
const OrganizationController = require("./controller/organization");
const logger = require("./lib/utils/logger");

app.use(express.json());
app.use(express.urlencoded({extended: false}));


app.post("/api/organization", OrganizationController.createOrganization);
app.get("/api/getOrganizations/:page", OrganizationController.getAllOrganization);
app.get("/api/searchOrganization", OrganizationController.searchOrganization);

// The Admin route to create users
require('./routes/api')(app)
require('./startUps')

const startServer = async () => {
    app.listen(port, (err) => {
        if (err) {
            logger.log(err.message);
            process.exit(1);
        }
        logger.log('info', `Server now listening on port  ${port}`);
    });
};
startServer();
