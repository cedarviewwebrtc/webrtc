# Cedarview webrtc

# Table of Contents

- Introduction
- Features
- Installation

## Introduction :

The app was simply built for webrtc app , but the first side of the project will be focused on the User Management profiles

## Features ( Routes)

1.  A private route was created for an Admin organization 
2.  A prIvate route was created for Admin users 
3.  Once the admin to an organization is created , a mail with link will be sent to the admin's email for a new Password change
4.  Once the admin click's on the link in his/her email, then it routes him to a page that he inputs his password and also confirms his/her password and click the submit button.
5.The page is posted to an updatePassword endpoint that authenticates the admin user  and ensures his email exist in the db . His/Her password is updated and status changed to active in the database. 

## Installation

Run npm install on your code environment to install the node_modules files
