"use strict"

const Joi = require('@hapi/joi');
const {errorResponse} = require('../lib/utils/helper')


exports.userInput = async (req, res, next) => {

    //this is the only data the frontend needs to pass for a user
    const schema = Joi.object({
        isAdmin: Joi.number().required(),
        email: Joi.string().email().max(256).required(),
        password: Joi.string().allow('').allow(null),
        status: Joi.string()
    });


    const {error} = schema.validate(req.body, {
        allowUnknown: true
    });

    if (error)
        return errorResponse(res, error.details[0].message.replace(/['"]/g, ''), 422)
    return next();

}

exports.changePasswordInput = async (req, res, next) => {
 //data the frontend needs to pass the new password for authenticated user
 const schema = Joi.object({
    newPassword: Joi.string().email().max(256).required(),
    confirmPassword: Joi.string().email().max(256).required()
    
});

const {error} = schema.validate(req.body, {
    allowUnknown: true
});

if (error)
    return errorResponse(res, error.details[0].message.replace(/['"]/g, ''), 422)
return next();
}