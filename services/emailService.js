"use strict"
require('dotenv').config()
const nodemailer = require('nodemailer')
const logger = require('./../lib/utils/logger')


let transporter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD
    }
});

let sendMail = (mailOptions)=>{
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
        return logger.log(error);
    }else {
        logger.log('Email sent: ' + info.response);
        return 'Email sent: ' + info.response
      }
  });
};

module.exports = sendMail;