function MakeOrganization() {
  this.presentParams = "";
}
MakeOrganization.prototype.requiredFields = (fieldList, params) => {
  presentParams = Object.keys(params);
  const failedList = [];
  fieldList.forEach((element) => {
    if (!presentParams.includes(element)) {
      failedList.push(element);
    }
    if (failedList.length > 0) {
      const message = `${failedList} is not present`;
      throw new Error(message);
    }
  });
  return params;
};
MakeOrganization.prototype.payloadType = (matchPhoneNumber, params) => {
  let testPhoneNumber = matchPhoneNumber.test(params.phoneNumber);
  let text = "";
  if (typeof params.name !== "string" || typeof params.name === "undefined") {
    text = "name is not of string type";
    throw new Error(text);
  }
  if (!testPhoneNumber) {
    text = "phoneNumber not a valid expression";
    throw new Error(text);
  }
  if (typeof params.address !== "string" || params.address.length < 4) {
    text = "address must be sring of minimum length 4";
    throw new Error(text);
  }
  return params;
};
MakeOrganization.prototype.paginationOffset = (page, pageSize) => {
  const offSet = pageSize * (page - 1);
  let limit = pageSize;
  return offSet;
};
module.exports = new MakeOrganization();
