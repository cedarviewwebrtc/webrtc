const db = require("../db/models");
const OrganizationRepository = require("../lib/repositories/organization/index");
const errorHandler = require("../lib/useException.");
const logger = require("../lib/utils/logger");

function OrganizationController() {
  this.headers = {
    "Content-Type": "application/json",
  };
  this.result = "";
}
OrganizationController.prototype.createOrganization = async (req = {}, res) => {
  const params = req.body;
  try {
    this.result = await OrganizationRepository.createOrganization(db, params);
    res.set(this.headers).status(201).json({ msg: this.result });
  } catch (error) {
    errorHandler(error, logger);
  }
};

OrganizationController.prototype.getAllOrganization = async (req = {}, res) => {
  const { page } = req.params;
  try {
    this.result = await OrganizationRepository.getAllOrganization(db, page);
    res.set(this.headers).status(200).json({ msg: this.result });
  } catch (error) {
    errorHandler(error, logger);
  }
};

OrganizationController.prototype.searchOrganization = async (req = {}, res) => {
  const params = req.body;
  try {
    this.result = await OrganizationRepository.searchOrganization(db, params);
    res.set(this.headers).status(200).json({ msg: this.result });
  } catch (error) {
    errorHandler(error, logger);
  }
};
module.exports = new OrganizationController();
