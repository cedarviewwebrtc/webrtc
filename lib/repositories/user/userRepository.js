const userModel = require('../../../db/models').user
const Repository = require('../../utils/Repository')


class UserRepository extends Repository{
     constructor(){
        super(userModel)
     }
     
     
}



module.exports = (new UserRepository());