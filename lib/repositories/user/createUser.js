"use strict"
const UserRepository = require('./userRepository')
const randomString = require('../../utils/helper')
const hashString = require('../../utils/helper')
const logger = require('../../utils/logger')
const jwt = require('../../utils/helper')
const {
    errorResponse,
    successResponse
} = require('../../utils/helper')
const mailer = require('../../../services/emailService')
const updatePassword = require('../../utils/messages')


exports.createUser = async (data, orgId) => {

    try {
        let {
            isAdmin,
            email,
            status
        } = data
        let randomChar = await randomString.generateString();
        let hashedPassword = await hashString.hashPassword(randomChar)
        let password = hashedPassword
        const userRecord = await UserRepository.create({
            isAdmin,
            email,
            password,
            status,
            orgId
        })
        // create accessToken for password update 
        const accessToken = await jwt.sign({
            orgId: userRecord.orgId,
            email: userRecord.email,
        }, process.env.Secret, {
            expiresIn: '24h'
        })
        // send mail to user using a link to change Password but authenticate the user first
        await mailer({
            from: process.env.EMAIL_USERNAME,
            to: userRecord.email,
            subject: 'change password',
            text: `your default password is ${userRecord.password}, update the password when you click on the link below!
            http://localhost:5000/api/authUser/token?token=${accessToken}`
        })
        return userRecord;
    } catch (err) {
        logger.log("error", err)
        return err
    }


}

exports.authenticateUser = async (token) => {
    try {
        const decoded = await jwt.verify(token, process.env.Secret)
        if(!decoded)
        return errorResponse(res, 'Unprocessed entity', 422);
        const userRecord = await UserRepository.find(decoded.orgId)
        if(userRecord.email !== decoded.email)
        return errorResponse(res, 'Credentials does not exist', 422);
        return userRecord;
    }catch(err){
        logger.log("error", err)
        return err
    }
  
}