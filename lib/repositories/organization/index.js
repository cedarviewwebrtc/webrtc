const createOrganization = require("./createOrganization");
const { getAllOrganization, searchOrganization } = require("./getOrganization");

class OrganizationRepository {
  static createOrganization(db, params) {
    return createOrganization(db, params);
  }
  static getAllOrganization(db, page) {
    return getAllOrganization(db, page);
  }
  static searchOrganization(db, params) {
    return searchOrganization(db, params);
  }
}
module.exports = OrganizationRepository;
