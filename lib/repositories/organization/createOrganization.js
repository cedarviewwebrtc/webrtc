const makeOrganization = require("../../../services/makeOrganization");
const { UniqueConstraintError } = require("../../utils/exception");
const { Op } = require("sequelize");


const createOrganization = async (db, params) => {
  const neededFields = ["name", "address", "phoneNumber"];
  const testPhoneNumber = /^\d{4,16}$/;
  makeOrganization.requiredFields(neededFields, params);
  makeOrganization.payloadType(testPhoneNumber, params);
  const uniqueCount = await db.organization.count({
    where: {
      [Op.or]: [{ name: params.name }, { phoneNumber: params.phoneNumber }],
    },
  });
  if (uniqueCount > 0) {
    throw new UniqueConstraintError("name or phone number must be unique");
  }
  const organizationRecord = await db.organization.create({
    name: params.name,
    phoneNumber: params.phoneNumber,
    address: params.address,
  });
  return organizationRecord;
};
module.exports = createOrganization;
