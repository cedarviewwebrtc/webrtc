const makeOrganization = require("../../../services/makeOrganization");
const { MissingResourceError } = require("../../utils/exception");


const getAllOrganization = async (db, page) => {
  const offset = makeOrganization.paginationOffset(page, (limit = 10));
  const allOrganization = await db.organization.findAll({ offset, limit });
  if (!allOrganization) {
    throw new MissingResourceError("No results found ");
  }
  return allOrganization;
};

const searchOrganization = async (db, params) => {
  const neededFields = ["name"];
  makeOrganization.requiredFields(neededFields, params);
  const getOrgByName = await db.organization.findOne({
    where: { name: params.name },
  });
  if (!getOrgByName) {
    throw new MissingResourceError(
      `This organization: ${params.name} does not exist`
    );
  }
  return getOrgByName;
};

module.exports = {
  getAllOrganization: getAllOrganization,
  searchOrganization: searchOrganization,
};
