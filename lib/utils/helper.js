'use strict'
const randomString = require('randomstring')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')



let successResponse = (res, data, code = 200) => {
    if ((data && data.docs)) {
        data.data = data.docs;
        delete data.docs;
        return res.status(code).json({data});
    }
    return res.status(code).json({data});
};


let errorResponse = (res, error = "Oops. An Error Occurred", code = 500) => {
    return res.status(code).json({error: error});
};


let generateString = () => {
    return randomString.generate({
        length: 10
    }) 
}

let hashPassword = async (payload) => {
 return ( await bcrypt.hashSync( 
        payload, 
       bcrypt.genSaltSync()
 ))
}

let sign = async (payload, secretKey, expires) => {
return jwt.sign(payload, secretKey, expires)
}

let verify = async (token, secretKey) => {
    return jwt.verify(token, secretKey);
}



module.exports = {
    successResponse,
    errorResponse,
    generateString,
    hashPassword,
    sign,
    verify
}