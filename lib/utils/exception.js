class UniqueConstraintError extends Error {
  constructor(message) {
    super(message);
    this.name = "UniqueConstraintError";
    this.message = message || "property value already exist";
  }
}

class MissingResourceError extends Error {
  constructor(message) {
    super(message);
    this.name = "MissingResourceError";
    this.message = message || "No results found";
  }
}

class ValidationError extends Error {
  constructor(message, statusCode) {
    super(message);
    (this.name = "ValidationError"),
      (this.message = message || "Missing field parameter"),
      (this.statusCode = statusCode || 400);
  }
}
class MissingParamtersError extends Error {
  constructor(message, statusCode) {
    super(message);
    this.name = "MissingParamtersError";
    this.message = message || "Missing field parameter";
  }
}

module.exports = {
  UniqueConstraintError: UniqueConstraintError,
  ValidationError: ValidationError,
  MissingParamtersError: MissingParamtersError,
  MissingResourceError: MissingResourceError,
};
