const { createLogger, format, transports, config } = require("winston");

const logger = createLogger({
  format: format.combine(format.timestamp(), format.json()),
  transports: [
    new transports.File({
      level: "info",
      name: "info-file",
      filename: "info.log",
    }),
  ],
  exceptionHandlers: [
    new transports.Console({
      level: "error",
    }),
    new transports.File({ level: "error", filename: "exceptions.log" }),
  ],
});

if (process.env.NODE_ENV !== "production") {
  logger.add(
    new transports.Console({
      format: format.combine(format.timestamp(), format.simple()),
    })
  );
}

module.exports = logger;
