"use strict"


class Repository {
    constructor(Model){
        this.Model = Model;
        this.create = this.create.bind(this);
        
    }


    create(payload){
        return this.Model.create(payload)
    }

    findAll(condition) {
        return this.Model.findAll({where: condition});
    }

    find(id) {
        return this.Model.findByPk(id);
    }

    findOne(condition){
        return this.Model.findOne({where: condition});
    }

    all (condition){
        return this.Model.findAll(condition);
    };

    update(condition, update) {
        return this.Model.update(update, {where: condition})
    }
}


module.exports = Repository;