const {
  UniqueConstraintError,
  MissingParamtersError,
  MissingResourceError,
  ValidationError,
} = require("./utils/exception");

const errorHandler = (error, logger) => {
  if (error instanceof UniqueConstraintError) {
    logger.info(error.message);
  } else if (error instanceof MissingParamtersError) {
    logger.info(error.message);
  } else if (error instanceof MissingResourceError) {
    logger.info("error", error.message);
  }
};

module.exports = errorHandler;
