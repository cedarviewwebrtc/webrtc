"use strict"

const router = require('express').Router()
const userValidate = require('../../middleware/userValidation')
const userController = require('../../controller/user')



//create user  
router.post('/', userValidate.userInput, userController.createUser)
//authenticate user with token
router.get('/', userController.userAuth)
//change password route
router.post('/', userValidate.changePasswordInput, userController.userPasswordUpdate)





module.exports = router;

