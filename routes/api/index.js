"use strict"

const userRoute = require('./user'),
      authenticateTokenRoute = require('./user'),
      passwordUpdateRoute = require('./user')






module.exports = (app) => {
    app.use("/api/users", userRoute)
    app.use("/api/authUser/token", authenticateTokenRoute)
    app.use("/api/changePassword", passwordUpdateRoute)
}


