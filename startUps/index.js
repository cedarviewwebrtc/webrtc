"usee strict"
const connection = require('./database')
const logger = require('../lib/utils/logger')



connection
.authenticate()
.then(() => {
  logger.log('info',`Connection has been established successfully.`);
})
.catch(err => {
  logger.log('error',`Unable to connect to the database:`);
});


module.exports =  {
  connection
 
}