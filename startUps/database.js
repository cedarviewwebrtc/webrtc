"use strict";
require('dotenv').config()

const mysql = require('mysql')
const { Sequelize } = require('sequelize');

// testing the connection to the db
const connection = new Sequelize('click2Call', process.env.Db_User, process.env.Db_Password, {
    host: process.env.Db_Host,
    dialect: 'mysql',
    pool: {
      max: 30,
      min: 0,
      acquire: 1000000,
      idle: 10000
  },
  define: {
      charset: 'utf8mb4',
      collate: 'utf8mb4_general_ci',
  }
    
  });




  module.exports = connection;