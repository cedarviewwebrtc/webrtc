'use strict';
module.exports = (sequelize, DataTypes) => {
  const organization = sequelize.define('organization', {
    name: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    address: DataTypes.STRING
  }, {});
  organization.associate = function(models) {
    // associations can be defined here
    organization.hasMany(models.user, { foreignKey: "orgId" });

  };
  return organization;
};